import argparse

_debug = False


directions_order = ['N', 'E', 'S', 'W']


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def turn(direction, facing, deg):
    idx = directions_order.index(facing)
    if direction == 'L':
        new_idx = (idx - (deg/90)) % 4
    else:
        new_idx = (idx + (deg/90)) % 4

    return directions_order[int(new_idx)]


def puzzle1(contents):
    moves = [ (line[:1], int(line[1:].rstrip())) for line in contents ]
    if _debug: print(moves)

    position = {'E': 0, 'W': 0, 'S': 0, 'N': 0}
    facing = 'E'

    for (instr, val) in moves:
        if instr == 'L' or instr == 'R':
            facing = turn(instr, facing, val)
        elif instr == 'F':
            position[facing] += val
        else:
            position[instr] += val
        
    manhattan = (abs(position['E'] - position['W']) + abs(position['N'] - position['S']))
    print(manhattan)
        

def puzzle2(contents):
    moves = [ (line[:1], int(line[1:].rstrip())) for line in contents ]
    if _debug: print(moves)

    ship = {'E': 0, 'W': 0, 'S': 0, 'N': 0}
    waypoint = {'N': 1, 'E': 10, 'S': 0, 'W': 0}
    facing = 'E'

    for (instr, val) in moves:
        if instr == 'L':
            shifts = int(val/90) 
            waypoint_vals = list(waypoint.values())
            for s in range(shifts):
                l = waypoint_vals.pop(0)
                waypoint_vals.append(l)
            for k in waypoint.keys():
                waypoint[k] = waypoint_vals.pop(0)

                # old_n = waypoint['N']
                # waypoint['N'] = waypoint['E']
                # waypoint['E'] = waypoint['S']
                # waypoint['S'] = waypoint['W']
                # waypoint['W'] = old_n
        elif instr == 'R':
            shifts = int(val/90) 
            waypoint_vals = list(waypoint.values())
            for s in range(shifts):
                r = waypoint_vals.pop()
                waypoint_vals.insert(0, r)
            for k in waypoint.keys():
                waypoint[k] = waypoint_vals.pop(0)

                # old_w = waypoint['W']
                # waypoint['W'] = waypoint['S']
                # waypoint['S'] = waypoint['E']
                # waypoint['E'] = waypoint['N']
                # waypoint['N'] = old_w
        elif instr == 'F':
            for k, v in waypoint.items():
                ship[k] += v * val
        else:
            waypoint[instr] += val
        
    manhattan = (abs(ship['E'] - ship['W']) + abs(ship['N'] - ship['S']))
    print(manhattan)
 


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
