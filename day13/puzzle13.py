import argparse
from functools import reduce


_debug = False


# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python_3.6
def chinese_remainder(n, a):
    # x = a mod n
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod
 
 
def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1
 

def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def puzzle1(contents):
    timestamp = int(contents[0].rstrip())
    ids = []
    for _id in contents[1].split(','):
        if not _id == 'x':
            ids.append(int(_id))
    
    waits = [ _id - timestamp % _id if timestamp % _id != 0 else 0 for _id in ids  ]
    min_wait = min(waits)
    if _debug: print(min_wait * ids[waits.index(min_wait)])
    return min_wait * ids[waits.index(min_wait)]


def puzzle2(contents):
    ids = []
    for _id in contents[1].split(','):
        if _id == 'x':
            ids.append(-1)
        else: ids.append(int(_id))

    # x = a mod n

    n = []
    a = []
    for idx, val in enumerate(ids):
        if val != -1:
            a.append(val - idx)
            n.append(val)
    
    print(chinese_remainder(n, a))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
