import argparse
import numpy as np
import math
import sys

_debug = False
np.set_printoptions(threshold=sys.maxsize)

def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def create_matrix_3d(contents, rounds):
    rounds = 6
    col_len = len(contents[0].rstrip()) + 2 * (rounds + 1)
    row_len = len(contents) + 2 * (rounds + 1)
    z_len = 1 + 2 * (rounds + 1)

    if _debug: print("dimensions:", col_len, row_len, z_len)

    pocket_dim = np.zeros((z_len, row_len, col_len))

    # place plane in middle
    pocket_row_start = math.floor(row_len/2) - math.floor(len(contents)/2)
    pocket_col_start = math.floor(col_len/2) - math.floor(len(contents[0].rstrip())/2)
    z = math.floor(z_len/2)

    y = pocket_col_start
    for line in contents:
        x = pocket_row_start
        elems = list(line.rstrip())
        for elem in elems:
            pocket_dim[z][y][x] = 0 if elem == '.' else 1
            if _debug: print("elem at", z, y, x, "is", pocket_dim[z][y][x])
            x += 1
        y += 1

    return pocket_dim


def create_matrix_4d(contents, rounds):
    rounds = 6
    col_len = len(contents[0].rstrip()) + 2 * (rounds + 1)
    row_len = len(contents) + 2 * (rounds + 1)
    z_len = 1 + 2 * (rounds + 1)
    w_len = 1 + 2 * (rounds + 1)

    if _debug: print("dimensions:", col_len, row_len, z_len, w_len)

    pocket_dim = np.zeros((w_len, z_len, row_len, col_len))

    # place plane in middle
    pocket_row_start = math.floor(row_len/2) - math.floor(len(contents)/2)
    pocket_col_start = math.floor(col_len/2) - math.floor(len(contents[0].rstrip())/2)
    z = math.floor(z_len/2)
    w = math.floor(w_len/2)

    y = pocket_col_start
    for line in contents:
        x = pocket_row_start
        elems = list(line.rstrip())
        for elem in elems:
            pocket_dim[w][z][y][x] = 0 if elem == '.' else 1
            if _debug: print("elem at", w, z, y, x, "is", pocket_dim[w][z][y][x])
            x += 1
        y += 1

    return pocket_dim


def check_neighbours_3d(pocket_dim, z, y, x):
    sum_n = 0
    cube = pocket_dim[z-1:z+2, y-1:y+2, x-1:x+2]
    if _debug: print("cube:", cube)
    sum_n = np.sum(cube)
    sum_n -= pocket_dim[z][y][x]
    if _debug: print("sum for", z, y, x, sum_n)
    return sum_n


def check_neighbours_4d(pocket_dim, w, z, y, x):
    sum_n = 0
    hypercube = pocket_dim[w-1:w+2, z-1:z+2, y-1:y+2, x-1:x+2]
    sum_n += np.sum(hypercube)
    sum_n -= pocket_dim[w][z][y][x]
    if _debug: print("hypercube:", hypercube)
    if _debug: print("sum for", w, z, y, x, sum_n)
    return sum_n


def puzzle1(contents):
    rounds = 6
    pocket_dim = create_matrix_3d(contents, rounds)
    new_pocket_dim = np.array(pocket_dim)
    for r in range(rounds):
        print("round:", r)
        # this could be restricted but I'm lazy
        for z in range(1, len(pocket_dim) - 1):
            for y in range(1, len(pocket_dim[z]) - 1):
                for x in range(1, len(pocket_dim[z][y])):
                    neigbs = check_neighbours_3d(pocket_dim, z, y, x)
                    if pocket_dim[z][y][x] == 0 and neigbs == 3:
                        new_pocket_dim[z][y][x] = 1
                    elif pocket_dim[z][y][x] == 1 and not 2 <= neigbs <= 3: 
                        new_pocket_dim[z][y][x] = 0
        pocket_dim = np.array(new_pocket_dim)
    print(np.sum(pocket_dim))


def puzzle2(contents):
    rounds = 6
    pocket_dim = create_matrix_4d(contents, rounds)
    new_pocket_dim = np.array(pocket_dim)
    for r in range(rounds):
        print("round:", r)
        # this could be restricted but I'm lazy
        for w in range(1, len(pocket_dim) - 1):
            for z in range(1, len(pocket_dim[w]) - 1):
                for y in range(1, len(pocket_dim[w][z]) - 1):
                    for x in range(1, len(pocket_dim[w][z][y])):
                        neigbs = check_neighbours_4d(pocket_dim, w, z, y, x)
                        if pocket_dim[w][z][y][x] == 0 and neigbs == 3:
                            new_pocket_dim[w][z][y][x] = 1
                        elif pocket_dim[w][z][y][x] == 1 and not 2 <= neigbs <= 3: 
                            new_pocket_dim[w][z][y][x] = 0
        pocket_dim = np.array(new_pocket_dim)
    print(np.sum(pocket_dim))






if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
