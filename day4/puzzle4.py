import re


def puzzle1(fields):
    with open('input.txt', 'r') as f:
        contents = (f.read()).split('\n\n')

    valid_passports = 0

    for _line in contents:
        _correct = True
        for _field in fields:
            if _line.find(_field) == -1:
                _correct = False
                break
        if _correct:
            valid_passports += 1
    print(valid_passports)


def puzzle2(fields):
    with open('input.txt', 'r') as f:
        contents = (f.read()).split('\n\n')

#    print(len(contents))

    valid_passports = 0
    eye_cols = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    for _line in contents:
        chunks = _line.split()
        elems = dict([c.split(":") for c in chunks])
        try:
            byr = len(elems['byr']) == 4 and int(elems['byr']) >= 1920 and int(elems['byr']) <= 2002
            iyr = len(elems['iyr']) == 4 and int(elems['iyr']) >= 2010 and int(elems['iyr']) <= 2020
            eyr = len(elems['eyr']) == 4 and int(elems['eyr']) >= 2020 and int(elems['eyr']) <= 2030
            hgt = (elems['hgt'][-2:] == 'cm' and int(elems['hgt'][:-2]) >= 150 and int(elems['hgt'][:-2]) <= 193) or \
                    (elems['hgt'][-2:] == 'in' and int(elems['hgt'][:-2]) >= 59 and int(elems['hgt'][:-2]) <= 76)
            hcl = (True if not re.match(r'^#[a-f0-9]{6}', elems['hcl']) is None else False) and len(elems['hcl']) == 7
            ecl = elems['ecl'] in eye_cols
            pid = len(elems['pid']) == 9 and (True if not re.match(r'^[0-9]{9}', elems['pid']) is None else False)
#            print('byr', byr, 'iyr', iyr, 'eyr', eyr, 'hgt', hgt, 'hcl', hcl, 'ecl', ecl, 'pid', pid)
            if byr and iyr and eyr and hgt and hcl and ecl and pid:
                valid_passports += 1
                print(elems)
#            else:
#                print('invalid\n')
        except KeyError:
#            print("missing keys\n")
            pass

    print(valid_passports)


if __name__ == '__main__':

    fields = ['ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt']
    puzzle2(fields)



