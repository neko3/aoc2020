from itertools import combinations
import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def get_all_sums_of_preamble(numbers):
    sums = set()
    comb = combinations(numbers, 2)
    for a, b in comb:
        sums.add(a+b)
    return sums


def puzzle1(numbers_list, preamble_len):

    for idx in range(preamble_len, len(numbers_list)):
        preamble = numbers_list[idx - preamble_len:idx]
        if _debug: print(preamble)
        sums = get_all_sums_of_preamble(preamble)
        if _debug: print("sums", sums, "n:", numbers_list[idx])
        if not numbers_list[idx] in sums:
            return idx

def sum_n(numbers_list, start_elem, n):
    sum = 0
    for idx in range(start_elem, star_elem + n):
        sum += numbers_list[idx]
    return sum

def puzzle2(numbers_list, weak_nbr_idx):

    if _debug: print('*' * 10, "PUZZLE 2")
    for n_elem in range(2, weak_nbr_idx):
        if _debug: print("n elem:", n_elem)
        for start_elem_idx in range(weak_nbr_idx - n_elem):
            if _debug: print("sum of", numbers_list[start_elem_idx:start_elem_idx+n_elem])
            s = sum(numbers_list[start_elem_idx:start_elem_idx+n_elem])
            if _debug: print(s)
            if s == numbers_list[weak_nbr_idx]:
                sum_min_max = min(numbers_list[start_elem_idx:start_elem_idx+n_elem]) + max(numbers_list[start_elem_idx:start_elem_idx+n_elem])
                return sum_min_max


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True


    contents = read_input('input.txt')
    numbers_list = [ int(n) for n in contents ]
    if _debug: print(numbers_list)
    weak_nbr_idx = puzzle1(numbers_list, 25)
    if _debug: print(weak_nbr_idx)
    print(numbers_list[weak_nbr_idx])
    weakness = puzzle2(numbers_list, weak_nbr_idx)
    print(weakness)
